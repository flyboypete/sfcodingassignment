# Submission for Coding Assignment - Peter Opheim - Senior Developer Candidate #
# peter.opheim@serenosoftware.com #
# 408-693-5478 #

This code contains the solution for the following requirements:

# ------------------------------------------------------------
Background
We've added a new picklist field Enterprise_Account_Status__c to Account and it has a default value of Bronze. This will ensure new records get a Bronze value, but the existing records still have a null value for Enterprise_Account_Status__c.  All Accounts with the Customer Account record type need to have Enterprise_Account_Status__c = Bronze.

Exercise 1
We need you to write a batch to update the existing Account records according to the specifications above. Include a test class to exercise the batch.

Considerations
We have 5 million Accounts in Salesforce, about a third are Customer Accounts
We only need this to happen once, but we often run into the broader issue of having to update different fields on different objects - can this be made generically enough to be reusable? For example, if we want any account with Gold_Account__c = true to have Enterprise_Account_Status__c = Gold, can we write the batch so it's reusable for this case as well?
# -----------------------------------------------------------


# SETUP #

This code requires the following modifications to the Account standard object:

Custom Fields:
Enterprise_Account_Status__c - Picklist - ['Gold','Silver','Bronze']
Is_Awesome__c - Checkbox

RecordTypes:
CustomerAccount - CustomerAccount
PartnerAccount - PartnerAccount


# RUNNING #

A batch class is used to modify the records as per the requirement, with five arguments supplied in the constructor to control its behavior.
These arguments are as follows:

ObjectName - String
(This is the API name of the SF object to update)

RecordTypeIds - List<String>
(This is a list of strings containing the IDs of the record types to limit the update to. Provide an empty list to update all)

QueryString - String
(This string allows additional filtering of the records to update, in SOQL format. Provide an empty string to update all)

FieldToUpdate - String
(This is the API name of the field on the object to update)

ValueToUpdate - String
(This is the value to update to. All values are supplied as strings, but type checked and parsed into proper data types)


# USAGE #

ObjectUpdateBatch(<ObjectName>, <RecordTypeIds>, <QueryString>, <FieldToUpdate>, <ValueToUpdate>);



This code can be executed by running the following three lines in anonymous apex. For example...

List<String> recordTypeIds = new List<String>{'0120H000001cmJ2','0120H000001cmJ7'};

ObjectUpdateBatch oub = new ObjectUpdateBatch('Account', recordTypeIds, 'Enterprise_Account_Status__c = \'Gold\'', 'Enterprise_Account_Status__c', 'Silver');

Database.executeBatch(oub);


The code is ~85% covered in its unit test...most code paths and exceptions are checked...there are however a few obvious holes.
There are also some data types that are not supported, but demonstrates the methodology in mind.

I feel that it is sufficent to demonstrate my level of competence with the platform and the understanding of the requirements.

Thanks for the consideration!

Cheers,

Peter