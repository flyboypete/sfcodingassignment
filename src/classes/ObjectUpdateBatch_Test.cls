@isTest
private class ObjectUpdateBatch_Test {

  @testSetup static void setupTest() {
    Map<String, Schema.RecordTypeInfo> rtiByName = 
      Schema.SObjectType.Account.getRecordTypeInfosByName();

    List<Account> newAccounts = new List<Account>();

    Account accnt1 = new Account();
    accnt1.Name = 'Account 1';
    accnt1.Enterprise_Account_Status__c = null;
    accnt1.Rating = 'Cold';
    accnt1.Is_Awesome__c = false;
    accnt1.recordtypeid = rtiByName.get('CustomerAccount').getRecordTypeId();
    newAccounts.add(accnt1);

    Account accnt2 = new Account();
    accnt2.Name = 'Account 2';
    accnt2.Enterprise_Account_Status__c = null;
    accnt2.Rating = 'Warm';
    accnt2.Is_Awesome__c = false;
    accnt2.recordtypeid = rtiByName.get('CustomerAccount').getRecordTypeId();
    newAccounts.add(accnt2);

    Account accnt3 = new Account();
    accnt3.Name = 'Account 3';
    accnt3.Enterprise_Account_Status__c = null;
    accnt3.Rating = 'Warm';
    accnt3.Is_Awesome__c = false;
    accnt3.recordtypeid = rtiByName.get('PartnerAccount').getRecordTypeId();
    newAccounts.add(accnt3);

    Account accnt4 = new Account();
    accnt4.Name = 'Account 4';
    accnt4.Enterprise_Account_Status__c = null;
    accnt4.Rating = 'Cold';
    accnt4.Is_Awesome__c = false;
    accnt4.recordtypeid = rtiByName.get('PartnerAccount').getRecordTypeId();
    newAccounts.add(accnt4);

    insert newAccounts;
  }
	
	@isTest static void testUpdateCustomerAccounts() {
    //Test base requirement
    Map<String, Schema.RecordTypeInfo> rtiByName = 
      Schema.SObjectType.Account.getRecordTypeInfosByName();

    List<String> recordTypeIds = new List<String>();
    recordTypeIds.add(rtiByName.get('CustomerAccount').getRecordTypeId());

    ObjectUpdateBatch obj = new ObjectUpdateBatch('Account', recordTypeIds, '', 'Enterprise_Account_Status__c', 'Bronze');

    Test.startTest();
    Database.executeBatch(obj);
    Test.stopTest();

    List<Account> accnts = [select Id, Name, Enterprise_Account_Status__c, RecordTypeId
                            from Account];

    for (Account accnt : accnts) {
      if (accnt.recordtypeid == rtiByName.get('CustomerAccount').getRecordTypeId()) {
        //Check that desired records were updated
        System.assertEquals('Bronze', accnt.Enterprise_Account_Status__c);
      } else {
        //Check that other records were unaffected
        System.assertEquals(null, accnt.Enterprise_Account_Status__c);
      }
    }
	}

  @isTest static void testUpdateColdRatingToGold() {
    Map<String, Schema.RecordTypeInfo> rtiByName = 
      Schema.SObjectType.Account.getRecordTypeInfosByName();

    List<String> recordTypeIds = new List<String>();

    ObjectUpdateBatch obj = new ObjectUpdateBatch('Account', recordTypeIds, 'Rating = \'Cold\'', 'Enterprise_Account_Status__c', 'Gold');

    Test.startTest();
    Database.executeBatch(obj);
    Test.stopTest();

    List<Account> accnts = [select Id, Name, Enterprise_Account_Status__c, Rating, RecordTypeId
                            from Account];

    for (Account accnt : accnts) {
      if (accnt.Rating == 'Cold') {
        //Check that desired records were updated
        System.assertEquals('Gold', accnt.Enterprise_Account_Status__c);
      } else {
        //Check that other records were unaffected
        System.assertEquals(null, accnt.Enterprise_Account_Status__c);
      }
    }
  }

  @isTest static void testUpdateAllToSilver() {
    Map<String, Schema.RecordTypeInfo> rtiByName = 
      Schema.SObjectType.Account.getRecordTypeInfosByName();

    List<String> recordTypeIds = new List<String>();

    ObjectUpdateBatch obj = new ObjectUpdateBatch('Account', recordTypeIds, '', 'Enterprise_Account_Status__c', 'Silver');

    Test.startTest();
    Database.executeBatch(obj);
    Test.stopTest();

    List<Account> accnts = [select Id, Name, Enterprise_Account_Status__c, RecordTypeId
                            from Account];

    for (Account accnt : accnts) {
      //Check that all records were updated
      System.assertEquals('Silver', accnt.Enterprise_Account_Status__c);
    }
  }

  @isTest static void testDecimalUpdate() {
    Map<String, Schema.RecordTypeInfo> rtiByName = 
      Schema.SObjectType.Account.getRecordTypeInfosByName();

    List<String> recordTypeIds = new List<String>();

    ObjectUpdateBatch obj = new ObjectUpdateBatch('Account', recordTypeIds, '', 'AnnualRevenue', '1000000.0');

    Test.startTest();
    Database.executeBatch(obj);
    Test.stopTest();

    List<Account> accnts = [select Id, Name, AnnualRevenue, RecordTypeId
                            from Account];

    for (Account accnt : accnts) {
      //Check that all records were updated
      System.assertEquals(1000000, accnt.AnnualRevenue);
    }
  }

  @isTest static void testBadDecimal() {
    Boolean exceptionCaught = false;

    List<String> recordTypeIds = new List<String>();

    try {
      Test.startTest();
      ObjectUpdateBatch obj = new ObjectUpdateBatch('Account', recordTypeIds, '', 'AnnualRevenue', 'NotANumber');
      Database.executeBatch(obj);
      Test.stopTest();
    } catch (Exception ex) {
      exceptionCaught = true;
    }
    System.assert(exceptionCaught);
  }

  @isTest static void testBooleanUpdate() {
    Map<String, Schema.RecordTypeInfo> rtiByName = 
      Schema.SObjectType.Account.getRecordTypeInfosByName();

    List<String> recordTypeIds = new List<String>();
    recordTypeIds.add(rtiByName.get('PartnerAccount').getRecordTypeId());

    ObjectUpdateBatch obj = new ObjectUpdateBatch('Account', recordTypeIds, '', 'Is_Awesome__c', 'true');

    Test.startTest();
    Database.executeBatch(obj);
    Test.stopTest();

    List<Account> accnts = [select Id, Name, Is_Awesome__c, RecordTypeId
                            from Account];

    for (Account accnt : accnts) {
      if (accnt.recordtypeid == rtiByName.get('PartnerAccount').getRecordTypeId()) {
        System.assertEquals(true, accnt.Is_Awesome__c);
      } else {
        System.assertEquals(false, accnt.Is_Awesome__c);
      }
    }
  }

  @isTest static void testBadBoolean() {
    Boolean exceptionCaught = false;

    List<String> recordTypeIds = new List<String>();

    try {
      Test.startTest();
      ObjectUpdateBatch obj = new ObjectUpdateBatch('Account', recordTypeIds, '', 'Is_Awesome__c', 'NotBoolean');
      Database.executeBatch(obj);
      Test.stopTest();
    } catch (Exception ex) {
      exceptionCaught = true;
    }
    System.assert(exceptionCaught);
  }

  @isTest static void testBadObjectName() {
    Boolean exceptionCaught = false;

    List<String> recordTypeIds = new List<String>();

    try {
      Test.startTest();
      ObjectUpdateBatch obj = new ObjectUpdateBatch('BadAccount', recordTypeIds, '', 'AnnualRevenue', '1000000.0');
      Database.executeBatch(obj);
      Test.stopTest();
    } catch (Exception ex) {
      exceptionCaught = true;
    }
    System.assert(exceptionCaught);
  }

  @isTest static void testBadFieldName() {
    Boolean exceptionCaught = false;

    List<String> recordTypeIds = new List<String>();

    try {
      Test.startTest();
      ObjectUpdateBatch obj = new ObjectUpdateBatch('Account', recordTypeIds, '', 'NotALotOfRevenue', '1000000.0');
      Database.executeBatch(obj);
      Test.stopTest();
    } catch (Exception ex) {
      exceptionCaught = true;
    }
    System.assert(exceptionCaught);
  }

  @isTest static void testNoAccount() {
    Boolean exceptionCaught = false;

    List<String> recordTypeIds = new List<String>();

    try {
      Test.startTest();
      ObjectUpdateBatch obj = new ObjectUpdateBatch('', recordTypeIds, '', 'AnnualRevenue', '1000000.0');
      Database.executeBatch(obj);
      Test.stopTest();
    } catch (Exception ex) {
      exceptionCaught = true;
    }
    System.assert(exceptionCaught);
  }

  @isTest static void testNoField() {
    Boolean exceptionCaught = false;

    List<String> recordTypeIds = new List<String>();

    try {
      Test.startTest();
      ObjectUpdateBatch obj = new ObjectUpdateBatch('Account', recordTypeIds, '', '', '1000000.0');
      Database.executeBatch(obj);
      Test.stopTest();
    } catch (Exception ex) {
      exceptionCaught = true;
    }
    System.assert(exceptionCaught);
  }

  @isTest static void testNoValue() {
    Boolean exceptionCaught = false;

    List<String> recordTypeIds = new List<String>();

    try {
      Test.startTest();
      ObjectUpdateBatch obj = new ObjectUpdateBatch('Account', recordTypeIds, '', 'AnnualRevenue', '');
      Database.executeBatch(obj);
      Test.stopTest();
    } catch (Exception ex) {
      exceptionCaught = true;
    }
    System.assert(exceptionCaught);
  }
}