public class ObjectUpdateBatch implements Database.Batchable<sObject>, Database.Stateful {
	
	private String sfObject;
	private List<String> recordTypeIds;
	private String queryCriteria;
	private String fieldToSet;
	private String valueToSet;

	private Schema.DisplayType fieldType;
	
	public ObjectUpdateBatch(String sfObject, List<String> recordTypeIds, 
		String queryCriteria, String fieldToSet, String valueToSet) {

		this.sfObject = sfObject;
		this.recordTypeIds = recordTypeIds;
		this.queryCriteria = queryCriteria;
		this.fieldToSet = fieldToSet;
		this.valueToSet = valueToSet;
	}
	
	public Database.QueryLocator start(Database.BatchableContext BC) {
		//Check for blank parameters
		if (String.isBlank(this.sfObject)) {
			throw new UpdateBatchException('SF Object parameter cannot be null');
		}
		if (String.isBlank(this.fieldToSet)) {
			throw new UpdateBatchException('Field to set parameter cannot be null');
		}
		if (String.isBlank(this.valueToSet)) {
			throw new UpdateBatchException('Value to set parameter cannot be null');
		}

		//Check for valid object and field name and get field type
		this.fieldType = getFieldType(this.sfObject, this.fieldToSet);

		
		 
		if (this.fieldType == Schema.DisplayType.String) {
		  //Do nothing. All strings are awesome. Could check for string lengths here though...
		} else if (this.fieldType == Schema.DisplayType.Picklist) {
		  //Could implement checking for valid picklist values
		} else if (this.fieldType == Schema.DisplayType.Boolean) {
		  if (this.valueToSet != 'true' && this.valueToSet != 'false') {
		  	throw new UpdateBatchException(
		  		'Value to set parameter should be boolean ("true"/"false")');
		  }
		} else if (this.fieldType == Schema.DisplayType.Integer) {
			if (!this.valueToSet.isNumeric()) {
		  	throw new UpdateBatchException(
		  		'Value to set parameter should be numeric');
		  }
		} else if (this.fieldType == Schema.DisplayType.Double || 
			this.fieldType == Schema.DisplayType.Currency ||
			this.fieldType == Schema.DisplayType.Percent) {

			if (!isDecimal(this.valueToSet)) {
				throw new UpdateBatchException(
					'Value to set parameter should be a decimal');
			}
		} else {
			throw new UpdateBatchException(
				'Update on type ' + this.fieldType + ' not supported...yet...');
		}

		String query = 'SELECT ' + this.fieldToSet + ' FROM ' + this.sfObject;

		if (this.recordTypeIds.size() > 0) {
			query += ' WHERE RecordTypeId in: recordTypeIds';
		}

		if (String.isNotBlank(this.queryCriteria) && this.recordTypeIds.size() > 0) {
			query += ' AND ' + this.queryCriteria;
		} else if (String.isNotBlank(this.queryCriteria) 
			&& this.recordTypeIds.size() == 0) {

			query += ' WHERE ' + this.queryCriteria;
		}
		System.debug(query);
		return Database.getQueryLocator(query);
	}

  public void execute(Database.BatchableContext BC, List<sObject> scope) {
  	Schema.SObjectType t = Schema.getGlobalDescribe().get(this.sfObject);
		Schema.DescribeSObjectResult r = t.getDescribe();
		Schema.DescribeFieldResult f = r.fields.getMap().get(this.fieldToSet)
			.getDescribe();

		for (sObject sObj : scope) {
			if (f.getType() == Schema.DisplayType.Boolean) {
				sObj.put(this.fieldToSet, Boolean.valueOf(this.valueToSet));
			} else if (f.getType() == Schema.DisplayType.Integer) {
				sObj.put(this.fieldToSet, Integer.valueOf(this.valueToSet));
			} else if (f.getType() == Schema.DisplayType.Double) {
				sObj.put(this.fieldToSet, Decimal.valueOf(this.valueToSet));
			} else if (f.getType() == Schema.DisplayType.Percent) {
				sObj.put(this.fieldToSet, Decimal.valueOf(this.valueToSet));
			} else if (f.getType() == Schema.DisplayType.Currency) {
				sObj.put(this.fieldToSet, Decimal.valueOf(this.valueToSet));
			} else {
				sObj.put(this.fieldToSet, this.valueToSet);
			}
		}

		update scope;
	}
	
	public void finish(Database.BatchableContext BC) {
		System.debug('Batch is finished');
	}

	//Helper functions
	private Schema.DisplayType getFieldType(String objectName, String fieldName) {
		if (Schema.getGlobalDescribe().get(objectName) == null) {
			throw new UpdateBatchException(
				'Object ' + objectName + ' does not exist in this org');
		}
		if (Schema.getGlobalDescribe().get(objectName).getDescribe()
			.fields.getMap().get(fieldName) == null) {

			throw new UpdateBatchException(
				'Field ' + fieldName + ' does not exist on object ' + objectName);
		}

		return Schema.getGlobalDescribe().get(objectName).getDescribe()
			.fields.getMap().get(fieldName).getDescribe().getType();
	}

	private Boolean isDecimal(String value) {
		Boolean isValidDecimal = true;

		if (value == null) {
			return false;
		}
    try {
       Decimal.valueOf(value);
    }
    catch(TypeException e){
      isValidDecimal = false; 
    }
    return isValidDecimal;
	}

	//Custom exception class to surface errors
	public class UpdateBatchException extends Exception {}
}